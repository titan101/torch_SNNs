import torch
import torch.nn as nn
import numpy as np
import time
import matplotlib
import matplotlib.pyplot as plt


from torch_SNN.base import waveform,time_stepped_object,net_base
from torch_SNN.utilities import make_waveform_table,raster_plot
from torch_SNN.layers import error_layer,poisson_layer,spike_layer,fully_connected_transformer
from torch_SNN.plasticity_rules import super_spike_rule

class one_layer_superspike_net(net_base):
      
    def __init__(self,target_spikes,n_poisson = 100,n_hidden = 4, seed = 42):
        super(one_layer_superspike_net, self).__init__()

	self.poisson_input = poisson_layer(n_poisson,batch_size = 1,rate = 40,deterministic=True,seed = seed)		   
	self.top = spike_layer(1,fully_connected_transformer())

	self.e_layer = error_layer(target_spikes)
	
	self.plasticity_rule_top = super_spike_rule(self.top)
	
	self.make_temporal_obj_list()
	
	
    def forward(self):
	src_spikes = self.poisson_input()
	top_spikes,top_vmem = self.top(src_spikes)	
	
	top_error = self.e_layer(top_spikes)

	self.plasticity_rule_top(src_spikes,top_vmem,top_error)
	
	
	self.advance_time_step()
	return top_spikes



time_step = 0.001
run_duration = 0.55
n_poisson = 100
n_hidden = 4
target_train_period = 0.1


n_steps = int(run_duration / time_step)
save_depth = n_steps


target = torch.ByteTensor(save_depth,1,1)
target.zero_()
inter = int(target_train_period/time_step)
target[inter::inter] = 1

net = one_layer_superspike_net(target,n_poisson,n_hidden)
net.set_time_step(time_step)
#run once to initialize waveforms using the dynamic allocation based on input sizes
net.forward()
net.reset()


#make waveform table
all_wfs = make_waveform_table(net)

#set save depth for desired waveforms
all_wfs['e_layer:alpha_filter_error:decay_filter:filtered_signal'].set_save_depth(save_depth)
all_wfs['e_layer:error_waveform'].set_save_depth(save_depth)
all_wfs['top:vmem'].set_save_depth(save_depth)
all_wfs['top:isyn'].set_save_depth(save_depth)
all_wfs['top:output_spikes'].set_save_depth(save_depth)
all_wfs['poisson_input:output_spikes'].set_save_depth(save_depth,[0,slice(2,5)])

filtered_error = all_wfs['e_layer:alpha_filter_error:decay_filter:filtered_signal']

net.cpu()
t1 = time.time()

n_trials = 50
start_beta = 1
end_beta = 10
beta_multiplier = (end_beta/start_beta) ** (1.0/(n_trials - 1))

current_beta = start_beta
for trial in range(n_trials):
    net.reset()
    for i in range(n_steps):
	net.plasticity_rule_top.beta = current_beta	
	net()
	net.plasticity_rule_top.commit_weight_update(0.001)	
    print 'in trial %d with beta %f : error %f ' % (trial,current_beta,filtered_error.wf[:,0,0].sum())
    current_beta *= beta_multiplier

plt.subplot(311)    
plt.plot(np.arange(save_depth)*time_step,all_wfs['top:vmem'].wf[:,0,0].cpu().numpy())    
plt.ylabel('top vmem')

plt.subplot(312)    
plt.plot(np.arange(save_depth)*time_step,all_wfs['e_layer:error_waveform'].wf[:,0,0].cpu().numpy())    
plt.ylabel('unfiltered_error')

plt.subplot(313)    
raster_plot(all_wfs['top:output_spikes'].wf,[0],0,time_step)    
plt.ylabel('top spikes')


plt.show()

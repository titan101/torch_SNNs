import torch
import torch.nn as nn
import numpy as np
import math
import torch.nn.functional as F
from torch.autograd import Variable


from base import waveform,time_stepped_object
from filters import double_exponential_filter

class error_layer(time_stepped_object):
      def __init__(self,target_spikes):
      	  super(error_layer, self).__init__()
	  self.register_buffer('target_spikes',target_spikes)
	  self.alpha_filter_error = double_exponential_filter()	  
	  
      def forward(self,x):
      	  if not(self.is_initialized()):
	      self.register_dynamical_variable('error_waveform',x.size(),torch.FloatTensor)
	      self.error_waveform.zero_()
	      
          cs = self.current_step
	  self.error_waveform[:] = (self.target_spikes[cs].float() - x.float() )

	  filtered_error_waveform = self.alpha_filter_error(self.error_waveform)
	  
	  return filtered_error_waveform
	  
class poisson_layer(time_stepped_object):
      def __init__(self, layer_shape,batch_size = 50,rate = 20,deterministic=False,seed = 42,pre_compute_size = 100):
      	    super(poisson_layer, self).__init__()
      	    self.rate = rate
	    self.p_per_step = self.rate * self.time_step
	    self.layer_shape = layer_shape if hasattr(layer_shape,'__iter__') else (layer_shape,)
	    
	    self.batch_size = batch_size
	    self.deterministic = deterministic
	    self.seed = seed
	    self.register_buffer('pre_computed_spikes',torch.ByteTensor(pre_compute_size,batch_size,*self.layer_shape))

	    self.initialize_own_rng_state()
	    self.generate_spikes()

      def initialize_own_rng_state(self):
	    torch.manual_seed(self.seed)
	    self.own_cpu_rng_state = torch.get_rng_state()
	    self.own_gpu_rng_state = torch.cuda.get_rng_state()
	    torch.manual_seed(np.random.randint(np.iinfo('int32').max))
		
	    
      def reset(self):
	  self.initialize_own_rng_state()
	  self.generate_spikes()
	  super(poisson_layer,self).reset()
	  
      def generate_spikes(self):
	  if self.deterministic:
	      torch.set_rng_state(self.own_cpu_rng_state)
	      torch.cuda.set_rng_state(self.own_gpu_rng_state)	      
	      self.pre_computed_spikes.bernoulli_(self.p_per_step)

	      self.own_cpu_rng_state = torch.get_rng_state()
	      self.own_gpu_rng_state = torch.cuda.get_rng_state()
	      torch.manual_seed(np.random.randint(np.iinfo('int32').max))
	  else:
	      self.pre_computed_spikes.bernoulli_(self.p_per_step)
	      
	  self.current_precomputed_index = 0

      def forward(self):
	  if not(self.is_initialized()):
	      self.register_dynamical_variable('output_spikes',torch.Size((self.batch_size,) + self.layer_shape),torch.ByteTensor)
	      self.output_spikes.zero_()
	      
	      
      	  self.output_spikes[:] = self.pre_computed_spikes[self.current_precomputed_index]
	  self.current_precomputed_index += 1

	  if (self.current_precomputed_index == self.pre_computed_spikes.size(0)):
	      self.generate_spikes()

	  return self.output_spikes


class spike_transformer(object):
      def __init__(self):
            self.target_dim = None

      def set_target_dim(self,target_dim):
            self.target_dim = target_dim


      def initialize_weight(self,src_dim,weight):
            raise NotImplementedError 

      def __call__(self,weight,x):
            raise NotImplementedError 


class fully_connected_transformer(spike_transformer):
      def __init__(self):
            super(fully_connected_transformer,self).__init__()
            self.target_dim = None


      def initialize_weight(self,src_dim,weight):
            in_features = src_dim[1]
            stdv = 2 * 1/math.sqrt(in_features)
	    weight.resize_(in_features,self.target_dim[0])
	    weight.normal_(0.0,stdv/50.0)

      def __call__(self,weight,x):
            return torch.matmul(x.float(),weight)

class conv_transformer(spike_transformer):
      def __init__(self,kernel_size = 3,stride = 1,padding = 2):
            super(conv_transformer,self).__init__()
	    self.stride = (stride,)*2
	    self.padding = (padding,)*2
            self.kernel_size = kernel_size
            self.target_dim = None
            
      def initialize_weight(self,src_dim,weight):
            in_features = self.kernel_size * self.kernel_size * src_dim[1]
            stdv = 2 * 1/math.sqrt(in_features)
	    weight.resize_(self.target_dim[0],src_dim[1],self.kernel_size,self.kernel_size)
	    weight.normal_(0.0,stdv/50.0)

      def __call__(self,weight,x):
            return F.conv2d(x.float(),weight,None,self.stride,self.padding).data
      
      
class spike_layer(time_stepped_object):
      def __init__(self, layer_shape,transformer,threshold = -0.05,tau_syn = 0.005,tau_mem = 0.01,t_ref = 0.005,v_rest = -0.06):
      	    super(spike_layer, self).__init__()

	    self.layer_shape = layer_shape if hasattr(layer_shape,'__iter__') else (layer_shape,)

	    self.threshold = threshold
	    self.tau_mem = tau_mem
	    self.tau_syn = tau_syn
	    self.t_ref = t_ref
	    self.v_rest = v_rest
	    self.weight_initialized = False
	    self.register_buffer('weight',torch.Tensor(1))

            self.transformer = transformer
            self.transformer.set_target_dim(self.layer_shape)
            
      def forward(self,input_spikes):
	  if not(self.is_initialized()):
	      batch_size = input_spikes.size(0)
	      self.register_dynamical_variable('vmem',torch.Size((batch_size,) + self.layer_shape),torch.FloatTensor)
	      self.register_dynamical_variable('isyn',torch.Size((batch_size,) + self.layer_shape),torch.FloatTensor)
	      self.register_dynamical_variable('output_spikes',torch.Size((batch_size,) + self.layer_shape),torch.ByteTensor)	      	      

	      self.isyn.zero_()
	      self.vmem.fill_(self.v_rest)
	      self.output_spikes.zero_()
	      if not(self.weight_initialized):
		  self.transformer.initialize_weight(input_spikes.size(),self.weight)
                  self.weight_initialized = True
	      
	  self.vmem.add_((self.isyn + self.v_rest - self.vmem) * (self.time_step/self.tau_mem))

	  self.isyn.add_(self.transformer(self.weight,input_spikes))

	  self.isyn.sub_(self.isyn * (self.time_step/ self.tau_syn))


	  self.output_spikes[:] = (self.vmem > self.threshold)

          
          self.vmem[self.output_spikes] = self.v_rest

	  
	  return self.output_spikes,self.vmem

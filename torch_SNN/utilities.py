from base import waveform
from base import waveform,time_stepped_object,net_base
import matplotlib.pyplot as plt
import numpy as np

def make_waveform_table(obj):
    update_name = lambda prepend,tab :  dict([(prepend + x,y) for x,y in tab.items()])
    waveform_table = obj.waveform_table.copy() if isinstance(obj,time_stepped_object) else {}
    for child_name,child in [(x,y) for (x,y) in obj._modules.items() if isinstance(y,time_stepped_object)]:
	child_table = make_waveform_table(child)
	waveform_table.update(update_name(child_name + ':',child_table))

    return waveform_table


def raster_plot(spike_train,neuron_indices,mini_batch_index,time_step):
    n_time_steps = spike_train.size(0)
    for i,idx in enumerate(neuron_indices):
	plt.scatter(np.arange(n_time_steps)*time_step,spike_train[:,mini_batch_index,idx].cpu().numpy() * (i+1))
    plt.ylim(ymin=0.5)
    plt.ylim(ymax = len(neuron_indices) + 0.5)
    

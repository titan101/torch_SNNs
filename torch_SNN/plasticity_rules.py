import torch
import torch.nn as nn

from base import waveform,time_stepped_object
from filters import double_exponential_filter

class super_spike_rule(time_stepped_object):
      def __init__(self,target_layer):
      	  super(super_spike_rule, self).__init__()

	  self.epsilon_filter = double_exponential_filter()
	  self.alpha_filter = double_exponential_filter()

	  self.target_layer = target_layer
	  self.beta = 10
	  
      def sigma_dot(self,x):
	  return (1+torch.abs(x))**-2

      def commit_weight_update(self,lr):
	  self.target_layer.weight.add_(self.delta_w * lr)
      
      def forward(self,src_spikes,target_vmem,filtered_error):
	  if not(self.is_initialized()):
	      self.register_dynamical_variable('delta_w',self.target_layer.weight.size(),torch.FloatTensor)
	      self.delta_w.zero_()
	      
	  filtered_src_spikes = self.epsilon_filter(src_spikes)

	  target_spike_derivative = self.sigma_dot(self.beta * (target_vmem - self.target_layer.threshold))

	  outer_prod = filtered_src_spikes.unsqueeze(2) * target_spike_derivative.unsqueeze(1)

	  filtered_outer_prod = self.alpha_filter(outer_prod)
	  

	  weight_update = (filtered_outer_prod * filtered_error.unsqueeze(1)).sum(0)


	  
	  self.delta_w =  weight_update

	  return torch.matmul(filtered_error,self.target_layer.weight.t())



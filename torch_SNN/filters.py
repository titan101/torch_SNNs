import torch
import torch.nn as nn
from base import time_stepped_object

class exponential_filter(time_stepped_object):
      def __init__(self,tau = 0.005):
      	  super(exponential_filter, self).__init__()
	  self.tau = tau


      def forward(self,x):
      	  if not(self.is_initialized()):
	      self.register_dynamical_variable('filtered_signal',x.size(),torch.FloatTensor)
	      self.filtered_signal.zero_()
	      
	  if x.type() == 'torch.ByteTensor' or x.type() == 'torch.cuda.ByteTensor':
	      self.filtered_signal.add_(x.float())
	      self.filtered_signal.sub_(self.filtered_signal * (self.time_step / self.tau))
	  else:
	      self.filtered_signal.add_(((x - self.filtered_signal) * (self.time_step/self.tau)))

	  return self.filtered_signal


class double_exponential_filter(time_stepped_object):
      def __init__(self,tau_rise = 0.005,tau_decay = 0.01):
      	  super(double_exponential_filter, self).__init__()

	  self.rise_filter = exponential_filter(tau = tau_rise)
	  self.decay_filter = exponential_filter(tau = tau_decay)	  

      def forward(self,x):
      	  return self.decay_filter(self.rise_filter(x))

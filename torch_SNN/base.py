import torch
import torch.nn as nn


class waveform():
    def __init__(self,name,dynamical_var,owner):
	self.dynamical_var = dynamical_var
	self.name = name
	self.owner = owner
        self.__save_depth = 0
        self.__current_save_idx = 0
        self.slices = None
        self.wf = None
        
    def update_dynamical_var(self,dynamical_var):
        self.dynamical_var = dynamical_var
        if self.wf is not None:
            self.wf.resize_(self.__save_depth,*(self.dynamical_var[self.slices].size()))
    def reset(self):
        self.__current_save_idx = 0
        
    def save(self):
        if self.__current_save_idx < self.__save_depth:
            #print 'saving ' + self.name + ' at step ' + repr(self.owner.current_step) + ' at idx ' + repr(self.__current_save_idx) + ' ' + repr(self.owner)
            self.wf[self.__current_save_idx] = self.dynamical_var[self.slices]
            self.__current_save_idx += 1

    def set_save_depth(self,sd,slices=None):
	if (sd < 1):
	    raise ValueError('Save depth can not be less than 1')
        self.slices =  slice(None) if slices is None else slices
        if self.wf is None:
            self.owner.register_buffer(self.name + '_wf',self.dynamical_var.new(sd,*(self.dynamical_var[self.slices].size())))
            self.wf = self.owner._buffers[self.name + '_wf']
        else:
            self.wf.resize_(sd,*(self.dynamical_var[self.slices].size()))
            if sd > self.__save_depth and self.__current_save_idx >= self.__save_depth:
                warnings.warn('extending waveform that has already run out of space. Save data has been lost in the inetrim')
            if sd < self.__current_save_idx:
                warnings.warn('Reducing save depth will result in loss of save data')
        self.__save_depth = sd

class time_stepped_object(nn.Module):
      __current_step = 0
      def __init__(self):
      	  super(time_stepped_object,self).__init__()

	  self.__initialized = False	  
          self.__time_step = 0.001
      

	  self.waveform_table = {}
	  #A hack to detect of the time-stepped object lives on GPU
	  #check the buffer is_cuda property
	  self.register_buffer('test_tensor',torch.Tensor(1))
	  
      @staticmethod
      def advance_current_step():
          time_stepped_object.__current_step += 1
      
      @property
      def time_step(self):
	  return self.__time_step

      @time_step.setter
      def time_step(self,ts):
          if self.is_initialized():
              raise RuntimeError('can not set time step unless object is reset')
	  self.__time_step = ts
      
      @property
      def current_step(self):
	  return time_stepped_object.__current_step
      

          
      def is_initialized(self):
	  return self.__initialized

      def reset(self):
	  self.__initialized = False
          time_stepped_object.__current_step = 0
          for wf in self.waveform_table.values():
              wf.reset()

      
      def save(self):
          [wf.save() for wf in self.waveform_table.values()]
	  
      def register_dynamical_variable(self,var_name,var_size,tensor_type):
	  if self.test_tensor.is_cuda:
	      self.register_buffer(var_name,tensor_type(*var_size).cuda())
	  else:
	      self.register_buffer(var_name,tensor_type(*var_size).cpu())

	  if var_name in self.waveform_table:
	      self.waveform_table[var_name].update_dynamical_var(self._buffers[var_name])
	  else:
	      self.waveform_table[var_name] = waveform(var_name,self._buffers[var_name],self)

	  self.__initialized = True 



class net_base(nn.Module):

    def __init__(self):
	super(net_base,self).__init__()

    def make_temporal_obj_list(self):
	self.temporal_obj_list = [x for x in self.modules() if isinstance(x,time_stepped_object)]

    def advance_time_step(self):
	[x.save() for x in self.temporal_obj_list]
        time_stepped_object.advance_current_step()

    def set_time_step(self,ts):
        for x in self.temporal_obj_list:
	    x.time_step = ts
        
    def reset(self):
	[x.reset() for x in self.temporal_obj_list]


          
